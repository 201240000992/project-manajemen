<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    {{-- css --}}
    <link rel="stylesheet" href="{{asset('dist/css/tabler.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/tabler-vendors.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/demo.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/tabler-vendors.min.css')}}">



</head>

<body>
    <div class="page">
        <div class="page-single">
            <div class="container">

                @yield('content')

            </div>
        </div>
    </div>

    <script src="{{asset('dist/js/demo.min.js')}}"></script>
    <script src="{{asset('dist/js/tabler.esm.min.js')}}"></script>
    <script src="{{asset('dist/js/demo.min.js')}}"></script>

</body>
</html>
