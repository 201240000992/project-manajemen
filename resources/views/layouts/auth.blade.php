<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    {{-- css tabler--}}
    <link rel="stylesheet" href="{{asset('dist/css/tabler.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/tabler-flags.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/tabler-payments.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/tabler-vendors.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/demo.min.css')}}">

    {{-- css select2 --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('select/css/select2.min.css') }}" rel="stylesheet" /> --}}


</head>
<body>

    <x-header></x-header>

    <div class="my-3 my-md-5">
        <div class="container">

            @yield('content')


        </div>
    </div>
    {{-- js select2 --}}
    {{-- note : jquery harus diurutan nomer 1 --}}
    {{-- <script src="{{asset('jquery.min.js')}}"></script> --}}
    <script src="{{asset('js/app.js')}}"></script>
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.select2multiple').select2({
                width: '100%'
                , dropdownParent: $("#Myreport")
            });
            $('.select2one').select2({
                width: '100%'
                , dropdownParent: $("#Myreport")
            });

            $('.select').select2({
                width: '100%'
            });

            $('#namauser').change(function() {
                var id = $(this).val();
                // $.ajax({
                //     type: "get",
                //     url: "https://api.rajaongkir.com/starter/city?province=5",
                //     data: "rajaongkir",
                //     dataType: "JSON",
                //     success: function (response) {
                //         console.log(response);
                //     }
                // });
            });
        });

    </script>

    {{-- js tabler --}}
    <script src="{{asset('dist/js/demo.min.js')}}"></script>

</body>
</html>
