@extends('layouts.auth')

@section('content')

<div class="row">
    {{-- start profile step --}}

    <div class="col-12 col-lg-4">
        <x-showproject>
            @slot('nama_project')
            {{ $project->nama_project }}
            @endslot
            @slot('harga')
            {{ $project->harga }}
            @endslot
            @slot('deadline')
            {{ $project->deadline }}
            @endslot
        </x-showproject>
    </div>
    {{-- end profile project  --}}


    <div class="col-12 col-lg-6">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-vcenter card-table">
                    <thead>
                        <tr>
                            <th>Name Langkah</th>
                            <th>Hapus</th>
                            <th>Status</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($steps as $step)
                        <tr>
                            <td>{{$step->nama_langkah}}</td>
                            <td>
                                <form action="{{route('step.destroy', $step)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn" type="submit">Hapus</button>
                                </form>
                            </td>
                            <td>
                                <form action="{{route('step.finished', $step)}}" method="post">
                                    @csrf
                                    <button class="btn" type="submit">{{($step->finished == 'Y') ? 'Selesai' : 'Belum' }}</button>
                                </form>
                            </td>
                            <td>
                                <a class="btn" data-bs-toggle="modal" data-bs-target="#step{{$step->id}}">
                                    Edit
                                </a>
                                <div class="modal modal-blur fade" id="step{{$step->id}}" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Langkah</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <form action="{{route('step.update', $step)}}" method="post">
                                                @csrf
                                                @method('put')
                                                <div class="modal-body row">
                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Nama Langkah</label>
                                                        <input type="text" class="form-control" name="nama_langkah" value="{{$step->nama_langkah}}" placeholder="Nama Project">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                                                        close modal
                                                    </a>
                                                    <button type="submit" class="btn btn-primary ms-auto">
                                                        + Buat
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>







@endsection
