@extends('layouts.auth')

@section( 'content')
<div class="row">
    {{-- start profile step --}}

    <div class="col-12 col-lg-4">
        <x-showproject>
            @slot('nama_project')
            {{ $project->nama_project }}
            @endslot
            @slot('harga')
            {{ $project->harga }}
            @endslot
            @slot('deadline')
            {{ $project->deadline }}
            @endslot
        </x-showproject>

        {{-- start kolom edit --}}
        <div class="card mt-3">
            <form action="{{route('folder.store', $project)}}" method="post">
                @csrf

                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">Masukkan Link</label>
                        <input type="text" class="form-control" name="deskripsi"  placeholder="Nama Project">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary ms-auto">
                        UPDATE
                    </button>
                </div>
            </form>
        </div>
        {{-- start kolom edit --}}
    </div>
    {{-- end profile project  --}}

    {{-- start kolom edit --}}
    <div class="col-12 col-lg-8">
        <div class="card">
            <form action="{{route('project.update', $project)}}" method="post">
                @csrf
                @method('put')
                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">Nama Project</label>
                        <input type="text" class="form-control" name="nama_project" value="{{ $project->nama_project }}" placeholder="Nama Project">
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <div class="form-label">Nama Pekerja</div>
                                <select class="form-select select" name="users[]" multiple>
                                    @foreach($users as $user)
                                    <option {{ $project->users()->find($user->id) ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <div class="form-label">Nama Pekerja</div>
                                <select class="form-select select" name="client_id">
                                    <option value=""> -- Pilih Client -- </option>
                                    @foreach($clients as $client)
                                    <option {{ $client->id == $project->client->id ? 'selected' : ' ' }} value="{{ $client->id }}">{{ $client->nama_client }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Harga</label>
                                <input type="text" name="harga" value="{{ $project->harga }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Deadline</label>
                                <input type="date" name="deadline" value="{{ $project->deadline }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label class="form-label">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" rows="3"> {{ $project->deskripsi }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary ms-auto">
                        UPDATE
                    </button>
                </div>
            </form>
        </div>
    </div>
    {{-- start kolom edit --}}

</div>
@endsection
