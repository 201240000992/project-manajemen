<div class="card">
    <div class="card-body p-4 text-center">
        <span class="avatar avatar-xl mb-3 avatar-rounded"> J </span>
        <h3 class="m-0 mb-1"> {{$nama_project}} </a></h3>
        <div class="text-muted">Rp. {{ $harga }} </div>
        <div class="mt-3">
            <span class="badge bg-purple-lt">{{ $deadline }}</span>
        </div>
    </div>
</div>