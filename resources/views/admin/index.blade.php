@extends('layouts.auth')

@section('content')

{{-- start modal untuk input project --}}
<a href="#" class="btn mb-3" data-bs-toggle="modal" data-bs-target="#Myreport">
    input Project
</a>

<div class="modal modal-blur fade" id="Myreport" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New report</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{route('project.store')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">Nama Project</label>
                        <input type="text" class="form-control" name="nama_project" placeholder="Nama Project">
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <div class="form-label">Nama Pekerja</div>
                                <select class="form-select select2multiple" name="users[]" multiple>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}"> {{$user->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <div class="form-label">Nama Client</div>
                                <select class="form-select select2one" id="namauser" name="client_id">
                                    <option value=""> -- Pilih Client -- </option>
                                    @foreach($clients as $client)
                                    <option value="{{$client->id}}">{{$client->nama_client}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Harga</label>
                                <input type="text" name="harga" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Deadline</label>
                                <input type="date" name="deadline" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label class="form-label">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                        Cancel
                    </a>

                    <button type="submit" class="btn btn-primary ms-auto">
                        Create new report
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end modal untuk input project --}}

{{-- start modal untuk input Client --}}
<a href="#" class="btn mb-3" data-bs-toggle="modal" data-bs-target="#Myclient">
    Input Client
</a>

<div class="modal modal-blur fade" id="Myclient" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New report</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{route('client.store')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">Nama Client</label>
                        <input type="text" class="form-control" name="nama_client" placeholder="Nama Client">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Nomor WA</label>
                                <input type="text" name="no_wa" placeholder="Nomor WA" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="text" name="email" placeholder="E-mail" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                        Cancel
                    </a>

                    <button type="submit" class="btn btn-primary ms-auto">
                        Create new report
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end modal untuk input Client --}}

{{-- start tabel finished payment --}}
<div class="col-12">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-vcenter card-table">
                <thead>
                    <tr>
                        <th>Nama Project</th>
                        <th>Nama Client</th>
                        <th>ID Transaksi</th>
                        <th>Tipe Pembayaran</th>
                        <th>Bank</th>
                        <th class="w-1"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $payment)
                    <tr>
                        <td>{{$payment->project_id}}</td>
                        <td class="text-muted"> {{$payment->client_id}}</td>
                        <td class="text-muted"><a href="#" class="text-reset">{{$payment->transaction_id}}</a></td>
                        <td class="text-muted">{{$payment->payment_type}}</td>
                        <td class="text-muted">{{$payment->bank ? : $payment->payment_type}}</td>
                        <td><a href="#">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- end tabel finished payment --}}

{{-- start tabel All project --}}
<div class="col-12 mt-3">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-vcenter card-table">
                <thead>
                    <tr>
                        <th>Nama Project</th>
                        <th>Harga</th>
                        <th>Nama Client</th>
                        <th>status link</th>
                        <th class="w-1"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                    <tr>
                        <td>{{$project->nama_project}}</td>
                        <td class="text-muted">
                            {{$project->harga}}
                        </td>
                        <td class="text-muted"><a href="#" class="text-reset"> {{$project->client->nama_client}} </a></td>
                        <td>
                            @php
                            echo (isset($project->folder)) ? '<i class=" fa-solid fa-check"></i>' : '<i class="fa-solid fa-xmark"></i>';
                            @endphp
                        </td>
                        <td>
                            <div class="btn-list flex-nowrap">
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle align-text-top" data-bs-toggle="dropdown">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end">
                                        <form action="{{route('project.destroy', $project)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="dropdown-item" type="submit">Hapus</button>
                                        </form>
                                        <a class="dropdown-item" href="{{route('project.edit', $project)}}">
                                            Edit
                                        </a>
                                        <form action="{{route('project.invoice', $project)}}" method="post">
                                            @csrf
                                            <button class="dropdown-item" type="submit"> Kirim </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- end tabel All project --}}

<div class="row mt-3">
    {{-- start tabel your Client --}}
    <div class="col-6">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-vcenter card-table">
                    <thead>
                        <tr>
                            <th>Name Client</th>
                            <th>Nomor Wa</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clients as $client)
                        <tr>
                            <td>{{$client->nama_client}}</td>
                            <td class="text-muted">
                                {{$client->no_wa}}
                            </td>
                            <td class="text-muted"><a href="#" class="text-reset">{{$client->email}}</a></td>
                            <td>
                                <div class="btn-list flex-nowrap">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle align-text-top" data-bs-toggle="dropdown">
                                            Actions
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <form action="{{route('client.destroy', $client)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button class="dropdown-item" type="submit">Hapus</button>
                                            </form>
                                            <a class="dropdown-item" href="{{route('client.show', $client)}}">
                                                Show / Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- end tabel your Client --}}

    {{-- start tabel your Pekerja --}}
    <div class="col-6">
        <div class="card">
            <div class="table-responsive">
                <table class="table table-vcenter card-table">
                    <thead>
                        <tr>
                            <th>Nama Pekerja</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th class="w-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td class="text-muted"><a href="#" class="text-reset">{{$user->email}}</a></td>
                            <td class="text-muted">{{$user->role}}</td>
                            <td>
                                <div class="btn-list flex-nowrap">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle align-text-top" data-bs-toggle="dropdown">
                                            Actions
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            {{-- <form action="{{route('project.destroy', $project)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="dropdown-item" type="submit">Hapus</button>
                                            </form> --}}
                                            {{-- <a class="dropdown-item" href="{{route('project.edit', $project)}}">
                                            Show/Edit
                                            </a> --}}
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- end tabel your Pekerja --}}
</div>


@endsection
