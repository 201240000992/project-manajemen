@extends('layouts.auth')
@section('content')



{{-- {{ $datas->count() }} --}}

<div class="my-3 my-md-5">
    <div class="container">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Links and buttons</h3>
                </div>
                <div class="list-group list-group-flush">
                    @foreach($datas as $data)
                    <a href="#" class="list-group-item list-group-item-action">{{ $data->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
