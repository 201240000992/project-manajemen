<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FolderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\StepController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'verified'], function () {
    Route::resource('client', ClientController::class);
    Route::resource('project', ProjectController::class);
    Route::post('project/invoice/{id}', [ProjectController::class, 'payment'])->name('project.invoice');
});
Route::get('/', DashboardController::class)->middleware(['CheckRole:alluser', 'auth']);

Route::get('/admin', [AdminController::class, 'index'])->middleware(['CheckRole:admin']);


// route::prefix('project')

// Route::resource('step', StepController::class);

Route::post('folder/{project}', [FolderController::class, 'store'])->name('folder.store');


Route::put('store/{project}', [StepController::class, 'store'])->name('step.simpan');

Route::post('step/{id}', [StepController::class, 'finished'])->name('step.finished');

Route::get('project/{project}', [ProjectController::class, 'projectstep'])->name('project.step');

Route::post('folder/save/{project}', [FolderController::class, 'store'])->name('folder.save');

// Route::resource('folder', FolderController::class);

Route::resource('step', StepController::class);

Route::delete('/user/{user}', [AdminController::class, 'destroy'])->name('user.destroy');

Route::get('/search', SearchController::class)->name('search');

Route::get('tabelproject', function () {
    return view('admin.project');
})->name('tabelproject');

Route::get('tabeluser', function () {
    return view('admin.user');
})->name('tabeluser');
