<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DataProject extends Mailable
{
    use Queueable, SerializesModels;


    public $details = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($detil)
    {
        $this->details = $detil; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->details;
        return $this->subject('INVOICE PROJECT')->view('send.payment', ['data' => $data]);
    }
}
