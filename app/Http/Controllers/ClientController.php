<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{

    public function index(Client $client)
    {
        $clients = Client::all();

        return view('client.index', [
            'clients' => $clients,
        ]);
    }

    public function store(Request $request)
    {

        // dd($request->all());
        $data = $request->validate([
            'nama_client' => 'required',
            'email' => 'required',
            'no_wa' => 'required'
        ]);
        Auth::user()->clients()->create($data);

        return back();
    }


    public function show(Client $client)
    {
        $projects = Project::get(['id', 'nama_project', 'harga', 'client_id', 'deadline']);
        return view('client.show', compact('client', 'projects'));
    }

    public function edit(Client $client)
    {
        return view('client.edit', ['client' => $client]);
    }

    public function update(Request $request, Client $client)
    {
        $client->update([
            'nama_client' => $request->nama_client,
            'no_wa' => $request->no_wa,
            'email' => $request->email,
        ]);
        return back();
    }

    public function destroy(Client $client)
    {
        $client->delete();
        return back();
    }
}
