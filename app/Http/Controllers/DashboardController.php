<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $user = Auth::user();
        $projects = $user->projects;
        $payments = $user->payments;

        return view(
            'dashboard',
            compact('projects', 'payments')
        );
    }
}
