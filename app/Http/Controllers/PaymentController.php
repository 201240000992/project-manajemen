<?php

namespace App\Http\Controllers;

use App\Mail\DataProject;
use App\Mail\SendProject;
use App\Models\Payment;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function store()
    {

        $params = [
            "enabled_payments" => [
                "credit_card", "cimb_clicks", "bca_klikbca", "bca_klikpay", "bri_epay", "echannel",
                "permata_va", "bca_va", "bni_va", "bri_va", "other_va", "gopay", "indomaret",
                "danamon_online", "akulaku", "shopeepay", "uob_ezpay"
            ],

            "transaction_details" => [
                "order_id" => 9,
                "gross_amount" => 10000
            ],
            "customer_details" => 1,

            "expiry" => [
                "start_time" =>  now()->format('Y-m-d H:i:s T'),
                "unit" => "days",
                "duration" => 1
            ],
        ];
        $url = 'https://app.sandbox.midtrans.com/snap/v1/transactions';

        $headers = [
            "Authorization" => "Basic " . base64_encode(config('payment.server_key')),
            "Accept" => "application/json",
            "Content-Type" =>  "application/json",
        ];

        $respon = Http::withHeaders($headers)->post($url, $params);

        return $respon;
    }


    public function notifikasi(Request $request)
    {
        $kode = substr($request->order_id, 9, 9);
        // $project = Project::find($request->order_id);

        if ($request->status_code == 200) {
            $project = Project::find($kode);
            Payment::create([
                'project_id' => $project->id,
                'user_id' => $project->user_id,
                'client_id' => $project->client_id,
                'terbayar' => 'S',
                'transaction_id' => $request->transaction_id,
                'status_code' => $request->status_code,
                'payment_type' => $request->payment_type,
                'merchant_id' => $request->merchant_id,
                'masked_card' => $request->masked_card,
                'currency' => $request->currency,
                'card_type' => $request->card_type,
                'bank' => $request->bank,
                'approval_code' => $request->approval_code
            ]);
            return $this->sendProject($project);
        }else{
            return response('error', 500, ['error']);
        }
    }
    public function sendProject(Project $project)
    {
        $client = $project->client;
        $folder = $project->folder;
        Mail::to($client->email)->send(new SendProject($folder));
        return back();
    }
}
