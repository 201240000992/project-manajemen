<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Step;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StepController extends Controller
{


    public function store(Request $request, Project $project)
    {
        $data = $request->validate([
            'nama_langkah' => 'required',
        ]);
        $data['finished'] = (isset($request->finished)) ? 'Y' : 'N';
        $project->steps()->create($data);
        return back();
    }


    public function update(Request $request, Step $step)
    {
        $data = $request->validate([
            'nama_langkah' => 'required',
        ]);
        $step->update($data);
        return back();
    }

    public function finished(Step $step, $id)
    {
        $data = $step->find($id);
        $retval = ($data->finished == 'Y') ? 'N' : 'Y';

        $data->update([
            'nama_langkah' => 'hai',
            'finished' => $retval,
        ]);

        return back();
    }

    public function destroy(Step $step)
    {
        $step->delete();
        return back();
    }
}
