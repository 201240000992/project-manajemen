<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $keyword = $request->keyword;
        if (! $keyword) {
            return redirect('back');
        }else{
            $datas = User::where('name', 'like', "%{$keyword}%")->get();
        }
        // dd($datas);

        return view('search', [
            'datas' => $datas
        ]);

        // dd($request->keyword);
    }
}
