<?php

namespace App\Http\Controllers;

use App\Mail\DataProject;
use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

use function PHPUnit\Framework\returnSelf;

class ProjectController extends Controller
{

    public function index()
    {
        $project = Auth::user()->projects;
        return view('project.index', ['project' => $project]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'nama_project' => 'required',
            'client_id' => 'required',
            'deskripsi' => 'required',
            'harga' => ['required', 'integer'],
            'deadline' => 'required',
        ]);
        $data['users'] = $request->users;
        $data['user_id'] = Auth::user()->id;
        $project = Project::create($data);
        $project->users()->sync($request->users);
        return back()->withStatus('Data Sudah Tersimpan');
    }

    public function show(Project $project)
    {
        $steps = $project->steps;
        $folder = $project->folder;
        return view('project.show', compact('steps', 'project', 'folder'));
    }

    public function edit(Project $project)
    {

        $clients = Client::get();
        $users = User::get();

        return view('project.edit', [
            'project' => $project,
            'clients' => $clients,
            'users' => $users
        ]);
    }

    public function update(Request $request, Project $project)
    {

        $data = $request->validate([
            'nama_project' => 'required',
            'client_id' => 'required',
            'deskripsi' => 'required',
            'harga' => ['required', 'integer'],
            'deadline' => 'required',
        ]);
        $project->update($data);
        $project->users()->sync($request->users);

        return redirect('admin')->withStatus('Data Berhasil Diupdate');
    }

    public function projectStep(Project $project)
    {


        $steps = $project->steps;

        return view('project.projectstep', [
            'project' => $project,
            'steps' => $steps
        ]);
    }
    public function destroy(Project $project)
    {

        $project->delete();
        return back();
    }

    public function midtransGateway($produk)
    {
        // dd($produk);
        $params = [
            "enabled_payments" => [
                "credit_card", "cimb_clicks", "bca_klikbca", "bca_klikpay", "bri_epay", "echannel",
                "permata_va", "bca_va", "bni_va", "bri_va", "other_va", "gopay", "indomaret",
                "danamon_online", "akulaku", "shopeepay", "uob_ezpay"
            ],

            "transaction_details" => [
                "order_id" => 'order_id-'.$produk->id,
                "gross_amount" => $produk->harga
            ],

            "item_details" => [
                'id' => $produk->id,
                'price' => $produk->harga,
                'quantity' => 1,
                'name' => $produk->nama_project
            ],
            "customer_details" => $produk->client_id,

            "expiry" => [
                "start_time" =>  now()->format('Y-m-d H:i:s T'),
                "unit" => "days",
                "duration" => 1
            ],
        ];
        $url = 'https://app.sandbox.midtrans.com/snap/v1/transactions';

        $headers = [
            "Authorization" => "Basic " . base64_encode(config('payment.server_key')),
            "Accept" => "application/json",
            "Content-Type" =>  "application/json",
        ];
        $respon = Http::withHeaders($headers)->post($url, $params);
        return $respon;
    }

    public function payment(Project $project, $id, Request $request)
    {
        $data = $project->find($id);
        $hai = $this->midtransGateway($data);

        // $hai = $this->midtransGateway($data->id, $data->harga, $data->client_id);
        return $this->SendMail($data, $hai);
    }

    public function SendMail($project, $respon)
    {
        // complete
        $data = json_decode($respon, true);
        $client = $project->client;
        // dd($client);
        Mail::to($client->email)->send(new DataProject($data));
        return back();
    }
}
