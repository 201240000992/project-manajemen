<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    use HasFactory;

    protected $fillable = ['nama_langkah', 'project_id', 'catatan', 'finished'];

    public function project()
    {
        $this->belongsTo(Project::class);
    }
}
