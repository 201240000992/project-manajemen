<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['nama_project', 'deskripsi', 'harga', 'deadline', 'client_id', 'user_id'];


    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function steps()
    {
        return $this->hasMany(Step::class);
    }
    public function folder()
    {
        return $this->hasOne(Folder::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    
}
