<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'client_id' => random_int(1, 10),
            'nama_project' => $this->faker->name(),
            'deskripsi' => $this->faker->word(),
            'harga' => random_int(1000, 99999),
            'deadline' => $this->faker->dateTime()
        ];
    }
}
